#include <Write.h>

#include <chrono>
#include <iostream>
#include <thread>

namespace Synchronization {
void Write::hello() {
  while (true) {
    std::unique_lock<std::mutex> lock{mtx_};
    condVar_.wait(lock, [this]() { return this->writeHello_; });
    std::cout << "Hello ";
    writeHello_ = false;
    condVar_.notify_one();
  }
}

void Write::world() {
  while (true) {
    std::unique_lock<std::mutex> lock{mtx_};
    condVar_.wait(lock, [this]() { return !(this->writeHello_); });
    std::cout << "World!" << std::endl;
    writeHello_ = true;
    condVar_.notify_one();
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
  }
}
} /* Synchronization */
