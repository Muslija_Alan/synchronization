#include <Write.h>

#include <thread>

using namespace Synchronization;

int main() {
  Write write;
  std::thread t1{[&write]() { write.world(); }};
  std::thread t2{[&write]() { write.hello(); }};

  t1.join();
  t2.join();
  return 0;
}
