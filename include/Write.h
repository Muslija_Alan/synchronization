#ifndef SYNCHRONIZATION_WRITE_H_
#define SYNCHRONIZATION_WRITE_H_
#include <condition_variable>
#include <mutex>

namespace Synchronization {
class Write {
  public:
  void hello();
  void world();

  private:
  std::mutex mtx_;
  std::condition_variable condVar_;
  bool writeHello_{true};
};
} /* Synchronization */
#endif /* ifndef SYNCHRONIZATION_WRITE_H_ */
